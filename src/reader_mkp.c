/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* This program is free software; you can redistribute it and/or             */
/* modify it under the terms of the GNU Lesser General Public License        */
/* as published by the Free Software Foundation; either version 3            */
/* of the License, or (at your option) any later version.                    */
/*                                                                           */
/* This program is distributed in the hope that it will be useful,           */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/* GNU Lesser General Public License for more details.                       */
/*                                                                           */
/* You should have received a copy of the GNU Lesser General Public License  */
/* along with this program; if not, write to the Free Software               */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.*/
/*                                                                           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/**@file   reader_mkp.c
 * @brief  MKP file reader for multiple knapsack problems
 * @author Martin Bergner
 */

/*---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0----+----1----+----2*/

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#if defined(_WIN32) || defined(_WIN64)
#else
#include <strings.h> /*lint --e{766}*/ /* needed for strcasecmp() */
#endif
#include <ctype.h>

#include "reader_mkp.h"
#include "probdata_binpacking.h"
#include "scip/scip.h"

#define READER_NAME             "mkpreader"
#define READER_DESC             "file reader for blocks corresponding to a mip in lpb format"
#define READER_EXTENSION        "mkp"



/*
 * Data structures
 */
#define MKP_MAX_LINELEN       65536
#define MKP_MAX_PUSHEDTOKENS  2

/** section in MKP File */
enum MkpSection
{
   MKP_START, MKP_NBLOCKS, MKP_BLOCKSIZES, MKP_BLOCKS, MKP_END
};
typedef enum MkpSection MKPSECTION;

enum MkpExpType
{
   MKP_EXP_NONE, MKP_EXP_UNSIGNED, MKP_EXP_SIGNED
};
typedef enum MkpExpType MKPEXPTYPE;


/** MKP reading data */
struct MkpInput
{
   SCIP_FILE*            file;               /**< file to read */
   char                  linebuf[MKP_MAX_LINELEN]; /**< line buffer */
   char*                 token;              /**< current token */
   char*                 tokenbuf;           /**< token buffer */
   char*                 pushedtokens[MKP_MAX_PUSHEDTOKENS]; /**< token stack */
   int                   npushedtokens;      /**< size of token stack */
   int                   linenumber;         /**< current line number */
   int                   linepos;            /**< current line position (column) */
   MKPSECTION            section;            /**< current section */
   SCIP_Bool             haserror;           /**< flag to indicate an error occurence */
   SCIP_HASHMAP*         vartoblock;         /**< hashmap mapping variables to blocks (1..nblocks) */
   SCIP_HASHMAP*         constoblock;        /**< hashmap mapping constraints to blocks (1..nblocks) */
};
typedef struct MkpInput MKPINPUT;

static const char delimchars[] = " \f\n\r\t\v";
static const char tokenchars[] = "-+:<>=";
static const char commentchars[] = "\\";


/*
 * Local methods (for reading)
 */

#define SORTTPL_NAMEEXT     LongInt
#define SORTTPL_KEYTYPE     SCIP_Longint*
#define SORTTPL_FIELD1TYPE  int
#include "scip/sorttpl.c" /*lint !e451*/

/** issues an error message and marks the MKP data to have errors */
static
void syntaxError(
   SCIP*                 scip,               /**< SCIP data structure */
   MKPINPUT*             mkpinput,           /**< MKP reading data */
   const char*           msg                 /**< error message */
   )
{
   char formatstr[256];

   assert(mkpinput != NULL);

   SCIPverbMessage(scip, SCIP_VERBLEVEL_MINIMAL, NULL, "Syntax error in line %d: %s ('%s')\n",
      mkpinput->linenumber, msg, mkpinput->token);
   if( mkpinput->linebuf[strlen(mkpinput->linebuf)-1] == '\n' )
   {
      SCIPverbMessage(scip, SCIP_VERBLEVEL_MINIMAL, NULL, "  input: %s", mkpinput->linebuf);
   }
   else
   {
      SCIPverbMessage(scip, SCIP_VERBLEVEL_MINIMAL, NULL, "  input: %s\n", mkpinput->linebuf);
   }
   (void) SCIPsnprintf(formatstr, 256, "         %%%ds\n", mkpinput->linepos);
   SCIPverbMessage(scip, SCIP_VERBLEVEL_MINIMAL, NULL, formatstr, "^");
   mkpinput->section  = MKP_END;
   mkpinput->haserror = TRUE;
}

/** returns whether a syntax error was detected */
static
SCIP_Bool hasError(
   MKPINPUT*             mkpinput            /**< MKP reading data */
   )
{
   assert(mkpinput != NULL);

   return mkpinput->haserror;
}

/** returns whether the given character is a token delimiter */
static
SCIP_Bool isDelimChar(
   char                  c                   /**< input character */
   )
{
   return (c == '\0') || (strchr(delimchars, c) != NULL);
}

/** returns whether the given character is a single token */
static
SCIP_Bool isTokenChar(
   char                  c                   /**< input character */
   )
{
   return (strchr(tokenchars, c) != NULL);
}

/** returns whether the current character is member of a value string */
static
SCIP_Bool isValueChar(
   char                  c,                  /**< input character */
   char                  nextc,              /**< next input character */
   SCIP_Bool             firstchar,          /**< is the given character the first char of the token? */
   SCIP_Bool*            hasdot,             /**< pointer to update the dot flag */
   MKPEXPTYPE*           exptype             /**< pointer to update the exponent type */
   )
{
   assert(hasdot != NULL);
   assert(exptype != NULL);

   if( isdigit(c) )
      return TRUE;
   else if( (*exptype == MKP_EXP_NONE) && !(*hasdot) && (c == '.') )
   {
      *hasdot = TRUE;
      return TRUE;
   }
   else if( !firstchar && (*exptype == MKP_EXP_NONE) && (c == 'e' || c == 'E') )
   {
      if( nextc == '+' || nextc == '-' )
      {
         *exptype = MKP_EXP_SIGNED;
         return TRUE;
      }
      else if( isdigit(nextc) )
      {
         *exptype = MKP_EXP_UNSIGNED;
         return TRUE;
      }
   }
   else if( (*exptype == MKP_EXP_SIGNED) && (c == '+' || c == '-') )
   {
      *exptype = MKP_EXP_UNSIGNED;
      return TRUE;
   }

   return FALSE;
}

/** reads the next line from the input file into the line buffer; skips comments;
 *  returns whether a line could be read
 */
static
SCIP_Bool getNextLine(
   MKPINPUT*             mkpinput            /**< MKP reading data */
   )
{
   int i;

   assert(mkpinput != NULL);

   /* clear the line */
   BMSclearMemoryArray(mkpinput->linebuf, MKP_MAX_LINELEN);

   /* read next line */
   mkpinput->linepos = 0;
   mkpinput->linebuf[MKP_MAX_LINELEN-2] = '\0';
   if( SCIPfgets(mkpinput->linebuf, sizeof(mkpinput->linebuf), mkpinput->file) == NULL )
      return FALSE;
   mkpinput->linenumber++;
   if( mkpinput->linebuf[MKP_MAX_LINELEN-2] != '\0' )
   {
      SCIPerrorMessage("Error: line %d exceeds %d characters\n", mkpinput->linenumber, MKP_MAX_LINELEN-2);
      mkpinput->haserror = TRUE;
      return FALSE;
   }
   mkpinput->linebuf[MKP_MAX_LINELEN-1] = '\0';
   mkpinput->linebuf[MKP_MAX_LINELEN-2] = '\0'; /* we want to use lookahead of one char -> we need two \0 at the end */

   /* skip characters after comment symbol */
   for( i = 0; commentchars[i] != '\0'; ++i )
   {
      char* commentstart;

      commentstart = strchr(mkpinput->linebuf, commentchars[i]);
      if( commentstart != NULL )
      {
         *commentstart = '\0';
         *(commentstart+1) = '\0'; /* we want to use lookahead of one char -> we need two \0 at the end */
      }
   }

   return TRUE;
}

/** reads the next token from the input file into the token buffer; returns whether a token was read */
static
SCIP_Bool getNextToken(
   MKPINPUT*             mkpinput            /**< MKP reading data */
   )
{
   SCIP_Bool hasdot;
   MKPEXPTYPE exptype;
   char* buf;
   int tokenlen;

   assert(mkpinput != NULL);
   assert(mkpinput->linepos < MKP_MAX_LINELEN);

   /* check the token stack */
   if( mkpinput->npushedtokens > 0 )
   {
      SCIPswapPointers((void**)&mkpinput->token, (void**)&mkpinput->pushedtokens[mkpinput->npushedtokens-1]);
      mkpinput->npushedtokens--;
      SCIPdebugMessage("(line %d) read token again: '%s'\n", mkpinput->linenumber, mkpinput->token);
      return TRUE;
   }

   /* skip delimiters */
   buf = mkpinput->linebuf;
   while( isDelimChar(buf[mkpinput->linepos]) )
   {
      if( buf[mkpinput->linepos] == '\0' )
      {
         if( !getNextLine(mkpinput) )
         {
            SCIPdebugMessage("(line %d) end of file\n", mkpinput->linenumber);
            return FALSE;
         }
      }
      else
         mkpinput->linepos++;
   }
   assert(mkpinput->linepos < MKP_MAX_LINELEN);
   assert(!isDelimChar(buf[mkpinput->linepos]));

   /* check if the token is a value */
   hasdot = FALSE;
   exptype = MKP_EXP_NONE;
   if( isValueChar(buf[mkpinput->linepos], buf[mkpinput->linepos+1], TRUE, &hasdot, &exptype) ) /*lint !e679*/
   {
      /* read value token */
      tokenlen = 0;
      do
      {
         assert(tokenlen < MKP_MAX_LINELEN);
         assert(!isDelimChar(buf[mkpinput->linepos]));
         mkpinput->token[tokenlen] = buf[mkpinput->linepos];
         ++tokenlen;
         ++(mkpinput->linepos);
         assert(mkpinput->linepos < MKP_MAX_LINELEN);
      }
      while( isValueChar(buf[mkpinput->linepos], buf[mkpinput->linepos+1], FALSE, &hasdot, &exptype) ); /*lint !e679*/
   }
   else
   {
      /* read non-value token */
      tokenlen = 0;
      do
      {
         assert(tokenlen < MKP_MAX_LINELEN);
         mkpinput->token[tokenlen] = buf[mkpinput->linepos];
         tokenlen++;
         mkpinput->linepos++;
         if( tokenlen == 1 && isTokenChar(mkpinput->token[0]) )
            break;
      }
      while( !isDelimChar(buf[mkpinput->linepos]) && !isTokenChar(buf[mkpinput->linepos]) );

   }
   assert(tokenlen < MKP_MAX_LINELEN);
   mkpinput->token[tokenlen] = '\0';

   return TRUE;
}

/** returns whether the current token is a value */
static
SCIP_Bool isInt(
   SCIP*                 scip,               /**< SCIP data structure */
   MKPINPUT*             mkpinput,           /**< MKP reading data */
   int*                  value               /**< pointer to store the value (unchanged, if token is no value) */
   )
{
   long val;
   char* endptr;

   assert(mkpinput != NULL);
   assert(value != NULL);
   assert(!strcasecmp(mkpinput->token, "INFINITY") == 0 && !strcasecmp(mkpinput->token, "INF") == 0 );

   val = strtol(mkpinput->token, &endptr, 0);
   if( endptr != mkpinput->token && *endptr == '\0' )
   {
      if( val < INT_MIN || val > INT_MAX ) /*lint !e685*/
         return FALSE;

      *value = (int) val;
      return TRUE;
   }


   return FALSE;
}

/** reads the blocks section */
static
SCIP_RETCODE readFile(
   SCIP*                 scip,               /**< SCIP data structure */
   MKPINPUT*             mkpinput            /**< MKP reading data */
   )
{
   int nitems;
   int nbins;
   int nweights;
   int ncapacities;
   int nprofits;
   SCIP_Longint* weights;
   SCIP_Longint* profits;
   SCIP_Longint* capacities;

   int* binids;
   int* ids;

   assert(mkpinput != NULL);

   nweights = 0;
   nprofits = 0;
   ncapacities = 0;

   SCIPdebugMessage("Reading file \n");
   if( !getNextToken(mkpinput) )
      syntaxError(scip, mkpinput, "Error reading nitems");

   if( isInt(scip, mkpinput, &nitems) )
   {
      SCIPdebugMessage("#items %d\n", nitems);
   }

   if( !getNextToken(mkpinput) )
      syntaxError(scip, mkpinput, "Error reading nbins");

   if( isInt(scip, mkpinput, &nbins) )
   {
      SCIPdebugMessage("#nbins %d\n", nbins);
   }

   SCIP_CALL( SCIPallocBufferArray(scip, &capacities, nbins) );
   SCIP_CALL( SCIPallocBufferArray(scip, &weights, nitems) );
   SCIP_CALL( SCIPallocBufferArray(scip, &profits, nitems) );
   SCIP_CALL( SCIPallocBufferArray(scip, &ids, nitems) );
   SCIP_CALL( SCIPallocBufferArray(scip, &binids, nbins) );

   while( nweights < nitems )
   {
      int weight;
      if( !getNextToken(mkpinput) )
         syntaxError(scip, mkpinput, "Error reading weights");

      if( isInt(scip, mkpinput, &weight) )
      {
         SCIPdebugMessage("Weight of item <%d> %d\n", nweights, weight);
         weights[nweights] = weight;
         ids[nweights] = nweights;
         ++nweights;
      }
   }

   while( nprofits < nitems )
   {
      int profit;
      if( !getNextToken(mkpinput) )
         syntaxError(scip, mkpinput, "Error reading profits");

      if( isInt(scip, mkpinput, &profit) )
      {
         SCIPdebugMessage("Profit of item <%d> %d\n", nprofits, profit);
         profits[nprofits] = profit;
         ++nprofits;
      }
   }

   while( ncapacities < nbins)
   {
      int capacity;
      if( !getNextToken(mkpinput) )
         syntaxError(scip, mkpinput, "Error reading capacities");

      if( isInt(scip, mkpinput, &capacity) )
      {
         SCIPdebugMessage("Capacity of bin <%d> %d\n", ncapacities, capacity);
         capacities[ncapacities] = capacity;
         binids[ncapacities] = ncapacities;
         ++ncapacities;
      }
   }


   SCIPsortLongInt(capacities, binids, nbins);

   SCIP_CALL( SCIPprobdataCreate(scip, "mkp", ids, weights, profits, nitems, binids, capacities, nbins) );


   SCIPfreeBufferArray(scip, &binids);
   SCIPfreeBufferArray(scip, &ids);
   SCIPfreeBufferArray(scip, &profits);
   SCIPfreeBufferArray(scip, &weights);
   SCIPfreeBufferArray(scip, &capacities);

   if( hasError(mkpinput) )
      return SCIP_READERROR;

   return SCIP_OKAY;

}


/** reads an MKP file */
static
SCIP_RETCODE readMKPFile(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_READER*          reader,             /**< reader data structure */
   MKPINPUT*             mkpinput,           /**< MKP reading data */
   const char*           filename            /**< name of the input file */
   )
{

   assert(scip != NULL);
   assert(reader != NULL);
   assert(mkpinput != NULL);
   assert(filename != NULL);

   /* open file */
   mkpinput->file = SCIPfopen(filename, "r");
   if( mkpinput->file == NULL )
   {
      SCIPerrorMessage("cannot open file <%s> for reading\n", filename);
      SCIPprintSysError(filename);
      return SCIP_NOFILE;
   }

   /* parse the file */

   SCIP_CALL( readFile(scip, mkpinput) );


   /* close file */
   SCIPfclose(mkpinput->file);

   return SCIP_OKAY;
}

/*
 * Callback methods of reader
 */

/** destructor of reader to free user data (called when SCIP is exiting) */
#define readerFreeMkp NULL

/** problem reading method of reader */
static
SCIP_DECL_READERREAD(readerReadMkp)
{
   SCIP_CALL( SCIPreadMkp(scip, reader, filename, result) );
   *result = SCIP_SUCCESS;
   return SCIP_OKAY;
}


/** problem writing method of reader */
static
SCIP_DECL_READERWRITE(readerWriteMkp)
{
   /*lint --e{715}*/
   //SCIP_CALL( writeMKPFile(scip, reader, file) );
   *result = SCIP_SUCCESS;
   return SCIP_OKAY;
}

/*
 * reader specific interface methods
 */

/** includes the mkp file reader in SCIP */
SCIP_RETCODE SCIPincludeReaderMkp(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   assert(scip != NULL);
   /* include lp reader */
   SCIP_CALL( SCIPincludeReader(scip, READER_NAME, READER_DESC, READER_EXTENSION,
         NULL, readerFreeMkp, readerReadMkp, readerWriteMkp, NULL) );

   return SCIP_OKAY;
}


/** reads problem from file */
SCIP_RETCODE SCIPreadMkp(
   SCIP*                 scip,               /**< SCIP data structure */
   SCIP_READER*          reader,             /**< the file reader itself */
   const char*           filename,           /**< full path and name of file to read, or NULL if stdin should be used */
   SCIP_RESULT*          result              /**< pointer to store the result of the file reading call */
   )
{
   SCIP_RETCODE retcode;
   MKPINPUT mkpinput;
   int i;

   /* initialize MKP input data */
   mkpinput.file = NULL;
   mkpinput.linebuf[0] = '\0';
   SCIP_CALL( SCIPallocMemoryArray(scip, &mkpinput.token, MKP_MAX_LINELEN) ); /*lint !e506*/
   mkpinput.token[0] = '\0';
   SCIP_CALL( SCIPallocMemoryArray(scip, &mkpinput.tokenbuf, MKP_MAX_LINELEN) ); /*lint !e506*/
   mkpinput.tokenbuf[0] = '\0';
   for( i = 0; i < MKP_MAX_PUSHEDTOKENS; ++i )
   {
      SCIP_CALL( SCIPallocMemoryArray(scip, &mkpinput.pushedtokens[i], MKP_MAX_LINELEN) ); /*lint !e506 !e866*/
   }

   mkpinput.npushedtokens = 0;
   mkpinput.linenumber = 0;
   mkpinput.linepos = 0;
   mkpinput.haserror = FALSE;

   /* read the file */
   retcode = readMKPFile(scip, reader, &mkpinput, filename);


   /* free dynamically allocated memory */
   SCIPfreeMemoryArray(scip, &mkpinput.token);
   SCIPfreeMemoryArray(scip, &mkpinput.tokenbuf);
   for( i = 0; i < MKP_MAX_PUSHEDTOKENS; ++i )
   {
      SCIPfreeMemoryArray(scip, &mkpinput.pushedtokens[i]);
   }

   /* evaluate the result */
   if( mkpinput.haserror )
      return SCIP_READERROR;
   else if( retcode == SCIP_OKAY)
   {
      *result = SCIP_SUCCESS;
   }

   return retcode;
}
